<?php

namespace DucPham\PriceDecimal\Plugin\Pricing\Render;

use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Amount
 * @package DucPham\PriceDecimal\Plugin\Pricing\Render
 */
class Amount
{
    /**
     * @param \Magento\Framework\Pricing\Render\Amount $subject
     * @param callable $proceed
     * @param $amount
     * @param bool $includeContainer
     * @param $precision
     * @return mixed
     */
    protected $priceCurrency;

    /**
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->priceCurrency  = $priceCurrency;
    }
    public function aroundFormatCurrency(
        \Magento\Framework\Pricing\Render\Amount $subject,
        \Closure $proceed,
        $amount,
        $includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION
    ) {
        $precision = 0;
        $proceed($amount, $includeContainer, $precision);
        return $this->priceCurrency->format($amount, $includeContainer, $precision);
    }
}

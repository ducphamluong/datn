<?php

namespace DucPham\PriceDecimal\Plugin;

class PriceCurrencyPlugin
{
    const PRECISION_ZERO = 0;

    public function beforeConvertAndRound(
        \Magento\Directory\Model\PriceCurrency $subject,
        ...$args
    ) {
        $args[1] = isset($args[1]) ? $args[1] : null;
        $args[2] = isset($args[2]) ? $args[2] : null;
        $args[3] = self::PRECISION_ZERO;
        return $args;
    }

    public function beforeFormat(\Magento\Directory\Model\PriceCurrency $subject, ...$args)
    {
//        $amount,
//        $includeContainer = true,
//        $precision = self::DEFAULT_PRECISION,
//        $scope = null,
//        $currency = null
        $args[2] = self::PRECISION_ZERO;
        return $args;
    }
    public function beforeConvertAndFormat(\Magento\Directory\Model\PriceCurrency $subject, ...$args)
    {
        $args[2] = self::PRECISION_ZERO;
        return $args;
    }
}

<?php
namespace DucPham\PriceDecimal\Plugin;

use Magento\Directory\Model\PriceCurrency;

/**
 * Class PriceCurrency
 * @package DucPham\PriceDecimal\Model
 */
class PriceCurrencyModel
{
    const PRECISION_ZERO = 0;

    public function beforeConvertAndRound(PriceCurrency $subject, ...$args)
    {
        $args[3] = self::PRECISION_ZERO;
        return $args;
    }
    public function beforeFormat(PriceCurrency $subject, ...$args)
    {
        $args[2] = self::PRECISION_ZERO;
        return $args;
    }
    public function beforeConvertAndFormat(PriceCurrency $subject, ...$args)
    {
        $args[2] = self::PRECISION_ZERO;
        return $args;
    }
    public function beforeRoundPrice(PriceCurrency $subject, ...$args)
    {
        $args[1] = self::PRECISION_ZERO;
        return $args;
    }
}

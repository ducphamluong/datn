Hướng dẫn cài đặt website trong DATN

Hướng dẫn cài đặt Magento 2.4.1 tham khảo từ trang chủ Magento https://devdocs.magento.com/guides/v2.4/install-gde/prereq/zip_install.html

1. Yêu cầu:
PHP 7.4
Mysql 5.6, 8.0 hoặc MariaDB 10.4
Apache 2.4
Elasticsearch
2. Cài đặt Apache
`sudo apt install apache2`

Tiến hành bật chạy apache2

```
sudo systemctl start apache2.service
sudo systemctl restart apache2.service
sudo systemctl enable apache2.service
sudo systemctl status apache2.service
```

3. Cài đặt mysql
`sudo apt install mysql-server mysql-client`

Tiến hành bật chạy MySQL

```
sudo systemctl start mysql.service
sudo systemctl restart mysql.service
sudo systemctl enable mysql.service
sudo systemctl status mysql.service
```
Cài xong tiến hành tạo user và database cho website
VD: user và database: datn password 123456

4. Tạo domain truy cập website

Sửa trong **/etc/hosts** thêm dòng
**127.0.0.1 datn.local**
để thêm domain mới
Tạo file **datn.local.conf** trong **/etc/apache2/sites-available/**

```
<VirtualHost *:80>
    ServerName datn.local
    ServerAlias datn.local
    ServerAdmin ducphamluong@gmail.com
    DocumentRoot /var/www/datn.local
    ErrorLog ${APACHE_LOG_DIR}/datn-error.log
    CustomLog ${APACHE_LOG_DIR}/datn-access.log combined
</VirtualHost>
```

Tiến hành enable site:

```
sudo a2ensite datn.local
sudo systemctl restart apache2
```

5. Cài đặt Elasticsearch

Tham Khảo cài đặt Elasticsearch từ trang chủ

Cài OpenJDK

```
sudo apt install openjdk-11-jdk -y
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch
```

Các câu lệnh để start, enable, xem status Elasticsearch

```
sudo systemctl start elasticsearch 
sudo systemctl status elasticsearch 
sudo systemctl enable elasticsearch
```

Kiểm tra cài đặt thành công hay chưa?
`curl -X GET 'http://datn.local:9200'`

6. Cài đặt Magento


Clone mã nguồn từ https://gitlab.com/ducphamluong/datn.git

Copy mã nguồn clone vào trong folder datn.local

Truy cập vào trong folder **var/www/datn.local**

```
cd var/www/datn.local
php bin/magento setup:install --base-url=http://datn.local/ \
--db-host=localhost --db-name=datn --db-user=datn --db-password=123456 \
--admin-firstname=Admin --admin-lastname=Admin --admin-email=ducphamluong@gmail.com \
--admin-user=admin --admin-password=admin123 --language=en_US \
--currency=USD --timezone=America/Chicago --use-rewrites=1 \
--search-engine=elasticsearch7 --elasticsearch-host=datn.local \
--elasticsearch-port=9200
```

Như vậy hoàn thành việc cài đặt website Magento 2
Tiến hành bật chạy các module 

`bin/magento setup:upgrade`
`bin/magento setup:di:compile`
`bin/magento setup:static-content:deploy`

Cài ModSecurity trong báo cáo đồ án


